package com.tao.rocketstream.producer;

import org.apache.rocketmq.common.message.MessageConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

/**
 * @author Administrator
 * @title: SenderService
 * @projectName SpringCloudStream
 * @description: TODO
 * @date 2021/1/1914:17
 */
@Service
public class SenderService {

    @Autowired
    private Source source;

    @Autowired
    private MySource mySource;

    public void send(String msg) throws Exception{
        boolean flag = source.output().send(MessageBuilder.withPayload(msg).build());
        System.out.println("消息发送==>"+flag );
    }


   public void send1(String msg) throws Exception{
        boolean flag = mySource.output1().send(MessageBuilder.withPayload(msg).build());
        System.out.println("消息发送output1==>"+flag );
    }

    public void send2(String msg) throws Exception{
        boolean flag = mySource.output2().send(MessageBuilder.withPayload(msg).build());
        System.out.println("消息发送output2==>"+flag );
    }


    public <T> void sendTransactionalMsg(T msg,int num){
        MessageBuilder builder = MessageBuilder.withPayload(msg)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .setHeader("test", String.valueOf(num));
        Message message=builder.build();
        mySource.outputTX().send(message);
    }


     public <T> void sendObject(T msg,String tag) throws Exception{
        Message message=MessageBuilder.withPayload(msg)
                .setHeader(MessageConst.PROPERTY_TAGS, tag)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build();

        boolean flag = mySource.output2().send(message);
        System.out.println("sendObject==>"+flag );
    }

}
