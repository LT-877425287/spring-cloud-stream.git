package com.tao.rocketstream.controller;

import com.alibaba.fastjson.JSONObject;
import com.tao.rocketstream.producer.SenderService;
import com.tao.rocketstream.service.IMessageProvider;
import com.tao.rocketstream.tx.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Administrator
 * @title: SendMessageController
 * @projectName SpringCloudStream
 * @description: TODO
 * @date 2021/1/1915:35
 */
@RestController
public class SendMessageController
{
    @Resource
    private IMessageProvider messageProvider;

    @Autowired
    private SenderService senderService;


    @Autowired
    private Sender sender ;


    @GetMapping(value = "/sendMessage")
    public String sendMessage()
    {
        return messageProvider.send();
    }

    @GetMapping(value = "/input")
    public String input(String msg) throws Exception {
        senderService.send(msg);
        return "ok";
    }

    @GetMapping(value = "/input1")
    public String input1(String msg) throws Exception {
        senderService.send1(msg);
        return "ok";
    }

    @GetMapping(value = "/input2")
    public String input2(String msg) throws Exception {
        senderService.send2(msg);
        return "ok";
    }

    @GetMapping(value = "/inputTX")
    public String inputTX(String msg) throws Exception {
        senderService.sendTransactionalMsg(msg+"1",1);
        senderService.sendTransactionalMsg(msg+"2",2);
        senderService.sendTransactionalMsg(msg+"3",3);
        senderService.sendTransactionalMsg(msg+"4",4);
        return "ok";
    }

    @GetMapping(value = "/sendObject")
    public String sendObject() throws Exception {
        JSONObject ss = new JSONObject();
        ss.put("name", "TAO");
        senderService.sendObject(ss,"myTag");
        return "ok";
    }





}
