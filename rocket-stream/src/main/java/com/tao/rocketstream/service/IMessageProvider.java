package com.tao.rocketstream.service;

/**
 * @author Administrator
 * @title: IMessageProvider
 * @projectName SpringCloudStream
 * @description: TODO
 * @date 2021/1/1915:32
 */
public interface IMessageProvider {
    public String send();

}
